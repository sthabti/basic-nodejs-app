exports.index =  function(req, res){
    res.render('index', { title: 'Home', heading: 'Welcome to my NodeJS Page', tagLine:"Just a NodeJS PlayGround"});
}

exports.about = function(req, res){
    res.render('about', { title: 'About Sabeur' });
}

